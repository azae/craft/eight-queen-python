# coding: utf-8

from queens import Board, QUEEN, EMPTY, generate_all_boards, explore_tree
import pytest


class TestCheckBoard():
    def test_good_board_shoud_return_true(self):
        board_rpr = ".Q.\n" +\
                    "...\n" +\
                    "Q..\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check() == True

    def test_bad_board_should_return_false(self):
        board_rpr = "Q.Q\n" +\
                    "...\n" +\
                    "Q..\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check() == False


class TestBoardClass():
    def test_queens_count(self):
        board_rpr = "Q.Q\n" +\
                    "...\n" +\
                    "Q..\n"
        board = Board.boardFromRep(board_rpr)
        assert board.queens == 3

    def test_should_parse_rpr(self):
        board_rpr = ".Q.\n" +\
                    "...\n" +\
                    "Q..\n"
        board = Board.boardFromRep(board_rpr)
        assert board.cell(line=1, col=2) == QUEEN
        assert board.cell(line=3, col=2) == EMPTY
        assert board.cell(line=3, col=1) == QUEEN
        assert board.lines == 3
        assert board.cols == 3


class TestSubCheck():
    def test_check_lines_should_return_false_when_more_than_one_queen_on_same_line(self):
        board_rpr = "Q.Q\n" +\
                    "...\n" +\
                    "...\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check_lines() == False

    def test_check_lines_shoud_return_true_when_only_one_queen_by_line(self):
        board_rpr = "Q..\n" +\
                    "...\n" +\
                    "Q..\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check_lines() == True

    def test_check_cols_should_return_false_when_more_than_one_queen_on_same_col(self):
        board_rpr = "..Q\n" +\
                    "..Q\n" +\
                    "...\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check_cols() == False

    def test_check_cols_shoud_return_true_when_only_one_queen_by_col(self):
        board_rpr = "...\n" +\
                    "...\n" +\
                    "Q.Q\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check_cols() == True

    def test_check_first_diag_should_return_false_when_more_than_one_queen_on_same_diag(self):
        board_rpr = ".Q.\n" +\
                    "..Q\n" +\
                    "...\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check_first_diag() == False

    def test_check_first_diag_shoud_return_true_when_only_one_queen_by_diag(self):
        board_rpr = "...\n" +\
                    "Q.Q\n" +\
                    "...\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check_first_diag() == True

    def test_check_second_diag_should_return_false_when_more_than_one_queen_on_same_diag(self):
        board_rpr = ".Q.\n" +\
                    "Q..\n" +\
                    "...\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check_second_diag() == False

    def test_check_second_diag_shoud_return_true_when_only_one_queen_by_diag(self):
        board_rpr = "...\n" +\
                    "Q.Q\n" +\
                    "...\n"
        board = Board.boardFromRep(board_rpr)
        assert board.check_second_diag() == True


class TestGenerateAll():
    def test_it_must_generate_all_permutations(self):
        assert len(set(generate_all_boards())) == 40320
        assert len(generate_all_boards()) == 40320


class TestExploreGraph():
    def test_should_return_board_when_ttl_is_zero(self):
        board_rpr = "...\n" +\
                    "...\n" +\
                    "...\n"
        actual = explore_tree(Board.boardFromRep(board_rpr), 0)
        assert str(actual[0]) == board_rpr

    def test_should_put_first_queen(self):
        board_rpr = "...\n" +\
                    "...\n" +\
                    "...\n"
        expected = "Q..\n" +\
            "...\n" +\
            "...\n"
        actual = explore_tree(Board.boardFromRep(board_rpr), 1)
        assert str(actual[0]) == expected

    def test_should_put_second_queen(self):
        board_rpr = "Q..\n" +\
                    "...\n" +\
                    "...\n"
        expected = "Q..\n" +\
            "Q..\n" +\
            "...\n"
        actual = explore_tree(Board.boardFromRep(board_rpr), 1)
        assert str(actual[0]) == expected

    def test_should_move_second_queen(self):
        board_rpr = "Q..\n" +\
                    "...\n" +\
                    "...\n"
        expected = "Q..\n" +\
            ".Q.\n" +\
            "...\n"
        actual = explore_tree(Board.boardFromRep(board_rpr), 1)
        assert str(actual[1]) == expected

    def test_should_build_tree_with_short_cut(self):
        board_rpr = "...\n" +\
                    "...\n" +\
                    "...\n"
        """
      Steps are :
    ...
    ...
    ...

    Q..            .Q.            ..Q
    ...            ...            ...
    ...            ...            ...

    X.. X.. Q..    .X. .X. .X.    ..Q ..X ..X
    X.. .X. ..Q    X.. .X. ..X    Q.. .X. ..X
    ... ... ...    ... ... ...    ... ... ...

    Q.. Q.. Q..                   ..Q ..Q ..Q
    ..Q ..Q ..Q                   Q.. Q.. Q..
    Q.. .Q. ..Q                   Q.. .Q. ..Q

    """
        expected = "..Q\n" +\
            "Q..\n" +\
            "Q..\n"
        actual = explore_tree(Board.boardFromRep(board_rpr), 3)
        assert len(actual) == 6
        assert str(actual[3]) == expected

    def test_should_stop_when_no_more_place_on_board(self):
        board_rpr = "...\n" +\
                    "...\n" +\
                    "...\n"
        actual = explore_tree(Board.boardFromRep(board_rpr), 30)
        assert len(actual) == 0

    def test_should_find_last_queen(self):
        board_rpr = "Q..\n" +\
                    "...\n" +\
                    "...\n"
        assert Board.boardFromRep(board_rpr).find_last_queen() == (1, 1)
        board_rpr = "Q..\n" +\
                    "..Q\n" +\
                    "...\n"
        assert Board.boardFromRep(board_rpr).find_last_queen() == (2, 3)
        board_rpr = "...\n" +\
                    "...\n" +\
                    "...\n"
        assert Board.boardFromRep(board_rpr).find_last_queen() == (0, 0)
