# coding: utf-8
from itertools import permutations
try:
    from string import maketrans
except ImportError:
    pass
from time import time


QUEEN = "Q"
EMPTY = "."


class Board():
    def __init__(self, binary_board):
        self._board = binary_board
        self._lines = len(self._board)
        self._cols = self._lines
        self._queens = self.count_queens()

    @staticmethod
    def boardFromRep(rep):
        board = Board(convert_to_binary(rep))
        return board

    def check(self):
        return self.check_lines() and self.check_cols() and self.check_first_diag() and self.check_second_diag()

    def cell(self, line, col):
        col_shift = self.cols - col
        col_mask = 1 << col_shift
        cell_value = (self._board[line - 1] & col_mask) >> col_shift
        if cell_value == 1:
            return QUEEN
        else:
            return EMPTY

    def set_cell(self, line, col, value):
        if value == QUEEN:
            col_mask = 1 << (self.cols - col)
            binary_board = list(self._board)
            binary_board[line - 1] = self._board[line - 1] | col_mask
            return Board(binary_board)
        else:
            col_mask = int(
                "".join(["1"]*(col-1) + ["0"] + ["1"]*(self.cols - col)), 2)
            binary_board = list(self._board)
            binary_board[line - 1] = self._board[line - 1] & col_mask
            return Board(binary_board)

    def find_last_queen(self):
        for line in range(self.lines, 0, -1):
            for col in range(self.cols, 0, -1):
                if self.cell(line, col) == QUEEN:
                    return (line, col)
        return (0, 0)

    @property
    def lines(self):
        return self._lines

    @property
    def cols(self):
        return self._cols

    @property
    def queens(self):
        return self._queens

    def count_queens(self):
        return sum([bin(line).count("1") for line in self._board])

    def check_lines(self):
        for line in self._board:
            if bin(line).count("1") > 1:
                return False
        return True

    def check_cols(self):
        last = 0
        for line in self._board:
            last = last ^ line
        if bin(last).count("1") == self.queens:
            return True
        return False

    def check_first_diag(self):
        shift_board = []
        for index, line in enumerate(self._board):
            shift_board.append(line << index)
        board = Board(shift_board)
        board._cols = len(shift_board) * 2
        return board.check_cols()

    def check_second_diag(self):
        shift_board = []
        for index, line in enumerate(self._board):
            shift_board.append((line << self.cols) >> index)
        board = Board(shift_board)
        board._cols = len(shift_board) * 2
        return board.check_cols()

    def __str__(self):
        outtab = QUEEN + EMPTY
        intab = "10"
        trantab = maketrans(intab, outtab)
        output = ""
        template = '0'+str(self.cols)+'b'
        for line in self._board:
            output += format(line, template).translate(trantab) + "\n"
        return output


def convert_to_binary(rep):
    board = []
    lines = rep.splitlines()
    for line in lines:
        board.append(convert_line(line))
    return board


def convert_line(line):
    intab = QUEEN + EMPTY
    outtab = "10"
    trantab = maketrans(intab, outtab)
    return int(line.translate(trantab), 2)


def generate_all_boards():
    board = []
    for line in generate_lines():
        board.append(line)
    return [Board(binary_board) for binary_board in permutations(board)]


def generate_lines():
    return [1, 2, 4, 8, 16, 32, 64, 128]


def explore_tree(board, ttl):
    if ttl == 0:
        return [board]

    if board.queens == board.cols:
        if board.check():
            return [board]
        return []

    return explore_childs(board, ttl)


def explore_childs(board, ttl):
    solutions = []
    line = board.queens
    for col in range(board.cols):
        new_board = board.set_cell(line + 1, col + 1, QUEEN)
        if board.check():
            solutions.extend(explore_tree(new_board, ttl - 1))
    return solutions


if __name__ == '__main__':
    start_time = time()
    count = 0
    for board in generate_all_boards():
        if board.check():
            count += 1
    end_time = time()
    print ('brut force : {} solutions in {:0.3f} ms'.format(
        count, (end_time-start_time)*1000.0))

    start_time = time()
    count = len(explore_tree(Board([0]*8), 10))
    end_time = time()
    print ('tree walk : {} solutions in {:0.3f} ms'.format(
        count, (end_time-start_time)*1000.0))
